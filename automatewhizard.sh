#!/bin/bash

# Variables
WORK_DIR=$(pwd)
HOMEDIR="/home/simon/Whizard/scripts"
RUNWHIZARD=${HOMEDIR}/runwhizard.sh
RUNWHIZARD_SHORT=${HOMEDIR}/runwhizardshort.sh
WHIZARD=${HOMEDIR}/../install/bin/whizard
EMAIL="brass@physik.uni-siegen.de"
BOOL_MKDIR=true
USE_SLURM=true

runwhizard () {
    local SINDARIN_FILE=${1}
    local WHIZARD_OPTIONS=${2:-}
    FILE_NAME=$(basename $SINDARIN_FILE .sin)
    DIR_NAME=$(dirname $SINDARIN_FILE)
    EXTENSION=${SINDARIN_FILE##*..}
    echo $EXTENSION
    createdirectory $FILE_NAME $FILE_NAME 0

    if [ $USE_SLURM ];  then
        echo 'running sbatch for WHIZARD'
        sbatch "--mail-user=${EMAIL} ${RUNWHIZARD} ${2} ../${SINDARIN_FILE}"
    else
        rm -f ${WORKDIR}/output.log
        nohup ${RUNWHIZARD} "${2} ../${SINDARIN_FILE}" 2>&1 > ${WORK_DIR}/output.log &
    fi
    cd $3
}

# $1 : Original dir
# $2 : Modifyied dir
# $3 : Modifying suffix
createdirectory () {
    set -x
    local orig_dir=$1
    local new_dir=$2
    local suffix=$3

    if [ $orig_dir == $new_dir ]; then
        printf -v new_dir "%s_%s" $orig_dir $suffix
    fi

    if [ -d $new_dir ]; then
        if [ "$BOOL_MKDIR" = true ] ; then
            suffix_new=$suffix
            let suffix_new+=1
            printf -v new_dir "%s_%s" $orig_dir $suffix
            createdirectory $1 $new_dir $suffix_new
        else
            cd $new_dir
            echo "Using existing $new_dir"
        fi
    else
        echo Creating $new_dir
        mkdir $new_dir
        cd $new_dir
        ln -sfn $new_dir ../$orig_dir
    fi
    set +x
}

while getopts ":w:o:s:eh" opt; do
    case $opt in
        h)
            echo "Usage: Sindarin-File"
            echo "-w <path-to-whizard>"
            echo "-s use 'short' line (2h) on cluster"
            echo "-e do not create a new directory"
            echo "-n do not use SLURM"
            ;;
        e)
            BOOL_MKDIR=false
            ;;
        l)
            RUNWHIZARD=${RUNWHIZARD_LONG}
            echo "using long line" >&2
            ;;
        n)
            echo "do not use SLURM"
            USE_SLURM=false
            ;;
        s)
            RUNWHIZARD=${RUNWHIZARD_SHORT}
            echo "using short line" >&2
            ;;
        w)
            WHIZARD=${OPTARG}
            echo "using WHIZARD in ${OPTARG}" >&2
            ;;
        \?)
            echo "Invalid option: -${OPTARG}" >&2
            exit 1
            ;;
        :)
            echo "Option -${OPTARG} requires an argument." >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND-1))

for sinfile in $@
do
    if [ -f $sinfile ]; then
        runwhizard $sinfile $WHIZARD $WORK_DIR
    else
        echo File $sinfile does not exists
    fi
done
