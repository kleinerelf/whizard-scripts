#!/bin/bash -x

pushd ${1}

for f in $(find . -name 'whizard.log' -or -name '*.tex'); do
    filename=$(basename ${f})
    extension="${filename##*.}"
    filename="${filename%.*}"
    dir=$(basename $(dirname ${f}))

    output="${dir}-history"
    if [ "${extension}"  == "tex" ]; then
        latex -interaction=nonstopmode -jobname=${output} ${f}
        whizard-gml "${filename}.mp"
        latex -interaction=nonstopmode -jobname=${output} ${f}
        dvips ${output}

        rm -f *.aux *.mp *.log *.dvi *.mpx *.ltp
    fi
done

popd
