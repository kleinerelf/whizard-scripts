#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = """Simon Braß (brass@physik.uni-siegen.de)"""

import codecs
import copy
import glob2
import pygraphviz as PG
import os.path
import re
import sys, getopt

class module:
    'Fortran Module dependency leaf class'

    name = ""
    filename = ""
    depends = []
    count_use = 0

    def __init__(self, name, filename):
        self.name = name
        self.filename = filename
        self.depends = []
        self.count_use = 0

    # def __init__(self, name, filename, depends):
    #     self.name = name
    #     self.filename = filename
    #     self.depends = depends

    # def __init__(self, name, filename, depends, count_use):
    #     self.name = name
    #     self.filename = filename
    #     self.depends = depends
    #     self.count_use = count_use

    def add_depend(self, name):
        self.depends.append(name)

    def set_count_use(self, count_use):
        self.count_use = count_use

    def __str__(self):
        return "Module:\n Name: {} used {}\n File: {} Depends on: {}\n".format(self.name, self.count_use, self.filename, self.depends)

    __repr__ = __str__

class modules:
    'Fortran Module dependency tree class'

    tree = []

    def __init__(self):
        #
        print("Init Module Tree")

    def add_module(self, module):
        self.tree.append(module)

    def __rm_duplicates(self):
        self.tree = list(set(self.tree))

    def __check_consistency(self):
        for leaf in self.tree:
            for module_name in leaf.depends:
                if not any(module.name == module_name for module in self.tree):
                    self.tree.append(module(module_name, ""))

    def count_depends(self):
        self.__rm_duplicates()
        self.__check_consistency()
        for leaf in self.tree:
            leaf.count_use = 0
            for module in self.tree:
                leaf.count_use += module.depends.count(leaf.name)

    def sort_tree(self):
        self.tree.sort(key=lambda v: v.depends.__len__())

    def __str__(self):
        string = ""
        for leaf in self.tree:
            string += str(leaf)

        return string

    __repr__ = __str__

def depends_on_modules(m, filename):
    is_in_module = False
    with codecs.open(filename, "r", encoding="utf-8", errors="surrogateescape") as ins:
        for line in ins:
            line = line.strip()

            if not line or line[0] == "!":
                continue

            if is_in_module and (re.search("end module", line) or re.search("implicit none", line)):
                is_in_module = False
                m.add_module(leaf)
                continue

            if not is_in_module and (re.search("(?<=^module\s)\w", line) and not re.search("procedure|end", line)):
                is_in_module = True

                tmp = module(re.split(" ", line)[1], filename)
                leaf = copy.deepcopy(tmp)
            elif is_in_module and (re.split("[ ,]", line)[0] == "use" and re.split("[ ,]", line)[1]):
                leaf.add_depend(re.split("[ ,]", line)[1])

    return modules

def main(argv):
    directory = ''
    outputfile = 'output' 
    try:
        opts, args = getopt.getopt(argv,"hd:o:",["dir=", "output="])
    except getopt.GetoptError:
        print("test.py -d <directory> -o <outputfile>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("depends.py: -d <directory>")
            sys.exit()
        elif opt in ("-d", "--dir"):
            directory = arg
        elif opt in ("-o", "--output"):
            outputfile = arg

    directory += "**/*.f90"
    inputfiles = glob2.glob(directory)

    exceptions = ["kinds", "constants", "unit_tests", "iso_varying_string", "ut", "uti"]
    m = modules()
    for inputfile in inputfiles:
        depends_on_modules(m, inputfile)

    m.count_depends()
    m.sort_tree()

    tree = PG.AGraph(directed=True, strict=True)
    tree.graph_attr['size'] = "200"
    tree.graph_attr['splines'] = "ortho"
    tree.node_attr['shape'] = "record"
    tree.edge_attr['dir'] = "back"
    for leaf  in m.tree:
        if not any([exception in leaf.name for exception in exceptions]):
            tree.add_node(leaf.name, label="{{ {}|{{ {} }}}}".format(leaf.name, os.path.split(leaf.filename)[1]))
            if leaf.depends.__len__():
                for depend in leaf.depends:
                    if not (depend in exceptions):
                        tree.add_edge(leaf.name, depend)

    tree.write(outputfile+".dot")
    tree.layout(prog="dot")
    tree.draw(outputfile+".png")
    print(m)

if __name__ == "__main__":
    main(sys.argv[1:])
