#! /bin/bash -x
# $Id:$
########################################################################


########################################################################
# Heuristics for the paths on our testing machines
########################################################################
# for d in \
#     /tmp/instant-whizard \
#     /scratch/ohl/instant-whizard \
#     /scratch2/ohl/instant-whizard \
#     `pwd`; do
#   if test -d $d; then
#    prefix=$d
#    break
#   fi
# done

prefix=`pwd`

FC="gfortran"
F77=$FC

counter=0

while getopts "ho:f:s" opt; do
    case ${opt} in
        f)
            echo "using FC=${OPTARG}"
            FC=${OPTARG}
            let "counter=counter + 2"
            ;;
        o)
            echo "using dir=${OPTARG}"
            prefix+=/${OPTARG%/}
            let "counter=counter + 2"
            ;;
        h)
            echo "Usage: [OPTIONS] "
            echo " -f fortran-compiler"
            echo " -o output directory"
            exit 1
            ;;
        ?)
            echo "invalid option"
            exit 1
            ;;
    esac
done

export FC
export F77

shift ${counter}

########################################################################
build_dir=$prefix/build
install_dir=$prefix/install
bin_dir=$install_dir/bin
########################################################################

########################################################################
# Try to work around a problem in Debian 7.0 (Wheezy)
########################################################################
if test -d /usr/lib/x86_64-linux-gnu; then
    LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu
    export LD_LIBRARY_PATH
    LIBRARY_PATH=/usr/lib/x86_64-linux-gnu
    export LIBRARY_PATH
    C_INCLUDE_PATH=/usr/include/x86_64-linux-gnu
    export C_INCLUDE_PATH
    CPLUS_INCLUDE_PATH=/usr/include/x86_64-linux-gnu
    export CPLUS_INCLUDE_PATH
fi

########################################################################

PATH=$install_dir/bin:$PATH
export PATH
LD_LIBRARY_PATH=$install_dir/lib:$install_dir/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
BOOST_ROOT=$install_dir/boost_1_59_0
export BOOST_ROOT

########################################################################
# The locations of the sofware packages
########################################################################

# Autotools
url_make=http://ftp.gnu.org/gnu/make/make-4.1.tar.gz
url_m4=http://ftp.gnu.org/gnu/m4/m4-latest.tar.gz # Stand 11/2015: 1.4.17
url_autoconf=http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz
url_automake=http://ftp.gnu.org/gnu/automake/automake-1.13.1.tar.gz
url_libtool=http://ftp.gnu.org/gnu/libtool/libtool-2.4.2.tar.gz

# GCC
url_gcc=ftp://ftp.mpi-sb.mpg.de/pub/gnu/mirror/gcc.gnu.org/pub/gcc/releases/gcc-5.2.0/gcc-5.2.0.tar.gz
url_openmpi=https://www.open-mpi.org/software/ompi/v1.10/downloads/openmpi-1.10.1.tar.gz
#url_gmp=ftp://ftp.gnu.org/gnu/gmp/gmp-4.3.2.tar.bz2
#url_gmp=ftp://ftp.gnu.org/gnu/gmp/gmp-5.1.1.tar.bz2  ## requires recent GCC
#url_mpfr=http://mpfr.loria.fr/mpfr-2.4.2/mpfr-2.4.2.tar.bz2
#url_mpfr=http://www.mpfr.org/mpfr-current/mpfr-3.1.1.tar.gz
#url_mpc=http://www.multiprecision.org/mpc/download/mpc-0.8.2.tar.gz
#url_mpc=http://www.multiprecision.org/mpc/download/mpc-1.0.1.tar.gz

# O'Caml
url_ocaml=http://caml.inria.fr/pub/distrib/ocaml-4.02/ocaml-4.02.3.tar.gz

# Whizard
url_whizard=http://www.hepforge.org/archive/whizard/whizard-2.2.7.tar.gz
url_hepmc=http://lcgapp.cern.ch/project/simu/HepMC/download/HepMC-2.06.09.tar.gz
# url_lhapdf=https://www.hepforge.org/archive/lhapdf/LHAPDF-6.1.5.tar.gz
url_lhapdf=http://www.hepforge.org/archive/lhapdf/lhapdf-5.9.1.tar.gz
url_boost=http://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.gz

########################################################################
# Heuristics for the optimal number of jobs
########################################################################
parallel_jobs=
if test -r /proc/cpuinfo; then
    n=`grep -c '^processor' /proc/cpuinfo`
    if test $n -gt 1; then
        parallel_jobs="-j `expr \( 3 \* $n \) / 2`"
    fi
fi

########################################################################

download () {
    url=$1
    name=`basename $1`
    test -r $name || wget $url
    test -r $name || curl $url -o $name
    test -r $name || exit 2
}

strip_tgz () {
    name=$1
    case $name in
        *.tgz) basename $name .tgz;;
        *.tar.gz) basename $name .tar.gz;;
        *.tar.bz2) basename $name .tar.bz2;;
    esac
}

untar () {
    name=$1
    dirname=`strip_tgz $name`
    if test ! -d $dirname; then
        case $name in
            *.gz|*.tgz) zcat $name;;
            *.bz2) bzcat $name;;
        esac | tar xf -
    fi
}

configure_make_install () {
    name=$1
    dirname=`strip_tgz $name`
    if ! [[ `md5sum -c $dirname.stamp` =~ OK ]]; then
        ( cd $dirname || exit 2
          ./configure --prefix=$install_dir
          make $parallel_jobs
          make install )
    fi
    md5sum $name > $dirname.stamp
}

########################################################################

build_generic () {
    url=$1
    name=`basename $url`
    download $url
    untar $name
    configure_make_install $name
}

build_gcc () {
    gcc_url=$1
    gcc_name=`basename $gcc_url`
    gcc_dirname=`strip_tgz $gcc_name`
    download $gcc_url
    untar $gcc_name
    if ! [[ `md5sum -c $gcc_dirname.stamp` =~ OK ]]; then
        ( cd $gcc_dirname || exit 2
          sh contrib/download_prerequisites
          ### Work around a strange "flex: exec failed" that
          ### occurs ONLY inside of Jenkins ....
          cd gmp || exit 2
          mv -v configure configure-orig
          sed "s,m4-not-needed,$bin_dir/m4," configure-orig > configure
          chmod +x configure )

        mkdir $gcc_dirname.build
        ( cd $gcc_dirname.build || exit 2
          ../$gcc_dirname/configure --prefix=$install_dir \
                                    --enable-languages='c,c++,fortran' \
                                    --disable-multilib
          ###       --disable-multiarch
          make bootstrap-lean $parallel_jobs
          make install )
    fi
    md5sum $gcc_name > $gcc_dirname.stamp
}

build_boost () {
    url=$1
    name=`basename $url`
    dirname=`strip_tgz $name`
    if ! [[ `md5sum -c $dirname.stamp` =~ OK ]];  then
        download $url
        untar $name
        ( cp $dirname $installdir/include -r)
    fi
    md5sum $name > $dirname.stamp
}

build_ocaml () {
    url=$1
    name=`basename $url`
    dirname=`strip_tgz $name`
    download $url
    untar $name
    if ! [[ `md5sum -c $dirname.stamp` =~ OK ]];  then
        ( cd $dirname || exit 2
          ./configure -prefix $install_dir
          ### The O'Caml Makefiles are not parallel-safe
          make world.opt
          make install )
    fi
    md5sum $name > $dirname.stamp
}

build_hepmc () {
    url=$1
    name=`basename $url`
    dirname=`strip_tgz $name`
    download $url
    untar $name
    if ! [[ `md5sum -c $dirname.stamp` =~ OK ]];  then
        ( cd $dirname || exit 2
          ./configure --prefix=$install_dir \
                      --with-momentum=GEV --with-length=MM
          make $parallel_jobs
          make check
          make install )
    fi
    md5sum $name > $dirname.stamp
}

build_whizard () {
    url=$1
    name=`basename $url`
    dirname=`strip_tgz $name`
    cp ../$name .
    download $url
    untar $name
    if ! [[ `md5sum -c $dirname.stamp` =~ OK ]]; then
        mkdir $dirname.build
        ( cd $dirname.build || exit 2
          HEPMC_DIR=$install_dir
          export HEPMC_DIR
          LHAPDF_DIR=$install_dir
          export LHAPDF_DIR
          ../$dirname/configure FC=$bin_dir/gfortran F77=$bin_dir/gfortran  --enable-fc-openmp --prefix=$install_dir
          make $parallel_jobs
          #      make check
          make install ) | tee whizard.log
    fi
    md5sum $name > $dirname.stamp
}

write_script () {
    name=$1
    date="`date`"
    myself="$0"
    rm -f $name
    cat <<EOF >$name
#! /bin/sh
########################################################################
# WHIZARD startup script,
#    created on $date
#    by $myself
########################################################################
PATH=$PATH
export PATH
LD_LIBRARY_PATH=$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
LIBRARY_PATH=$LIBRARY_PATH
export LIBRARY_PATH
########################################################################
exec $install_dir/bin/whizard "\$@"
EOF
    chmod 555 $name
}

########################################################################

function main() {
    local run="$1"
    shift

    log_dir=log
    mkdir -p $log_dir
    cd $build_dir || (echo "cd: failed to go to ${log_dir}" && exit 2)

    mkdir -p $build_dir
    cd $build_dir || (echo "cd: failed to go to ${build_dir}" && exit 2)

    case ${run} in
        clean)
            rm -f $build_dir/*.stamp
            ;;
        build_generic)
            build_generic $url_make 2>&1 | tee ${log_dir}/make.log
            build_generic $url_m4 2>&1 | tee ${log_dir}/m4.log
            build_generic $url_autoconf 2>&1 | tee ${log_dir}/autoconf.log
            build_generic $url_automake 2>&1 | tee ${log_dir}/automake.log
            build_generic $url_libtool 2>&1 | tee ${log_dir}/libtool.log
            ;;
        build_gcc)
            build_gcc $url_gcc 2>&1 | tee ${log_dir}/gcc.log
            ;;
        build_openmpi)
            build_generic $url_openmpi 2>&1 | tee ${log_dir}/openmpi.log
            ;;
        build_ocaml)
            build_ocaml $url_ocaml 2>&1 | tee ${log_dir}/ocaml.log
            ;;
        build_hepmc)
            build_hepmc $url_hepmc 2>&1 | tee ${log_dir}/hepmc.log
            ;;
        build_lhapdf)
            build_boost $url_boost 2>&1 | tee ${log_dir}/boost.log
            build_generic $url_lhapdf 2>&1 | tee ${log_dir}/lhapdf.log

            $bin_dir/lhapdf-getdata --dest=`$bin_dir/lhapdf-config --pdfsets-path` \
                                    cteq61.LHpdf cteq6ll.LHpdf cteq5l.LHgrid GSG961.LHgrid
            # for lhapdf_set in cteq6 cteq6l1 cteq66; do
            #   wget http://www.hepforge.org/archive/lhapdf/pdfsets/6.1/${lhapdf_set}.tar.gz -O- | \
            #       tar xz -C $install_dir/share/LHAPDF
            # done
            ;;
        build_whizard)
            build_whizard $url_whizard 2>&1 | tee ${log_dir}/whizard.log
            write_script $bin_dir/whizard.sh
            ;;
        *)
            build_generic $url_make 2>&1 | tee ${log_dir}/make.log
            build_generic $url_m4 2>&1 | tee ${log_dir}/m4.log
            build_generic $url_autoconf 2>&1 | tee ${log_dir}/autoconf.log
            build_generic $url_automake 2>&1 | tee ${log_dir}/automake.log
            build_generic $url_libtool 2>&1 | tee ${log_dir}/libtool.log
            build_gcc $url_gcc 2>&1 | tee ${log_dir}/gcc.log
            build_generic $url_openmpi 2>&1 | tee ${log_dir}/openmpi.log
            build_ocaml $url_ocaml 2>&1 | tee ${log_dir}/ocaml.log
            build_hepmc $url_hepmc 2>&1 | tee ${log_dir}/ hepmc.log
            build_boost $url_boost 2>&1 | tee ${log_dir}/boost.log
            build_generic $url_lhapdf 2>&1 | tee ${log_dir}/lhapdf.log

            $bin_dir/lhapdf-getdata --dest=`$bin_dir/lhapdf-config --pdfsets-path` \
                                    cteq61.LHpdf cteq6ll.LHpdf cteq5l.LHgrid GSG961.LHgrid
            # for lhapdf_set in cteq6 cteq6l1 cteq66; do
            #     wget http://www.hepforge.org/archive/lhapdf/pdfsets/6.1/${lhapdf_set}.tar.gz -O- | \
            #   tar xz -C $install_dir/share/LHAPDF
            # done
            build_whizard $url_whizard | tee ${log_dir}/whizard.log

            write_script $bin_dir/whizard.sh
            echo "Nothing more to do :)."
            ;;
    esac
}

main "$@"
