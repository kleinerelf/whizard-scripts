#!/bin/bash -x

WORK_DIR=${1:-"whizard-work"}

function make_latex() {
    local output=${1}
    local texfile=${2}
    latex -interaction=nonstopmode -jobname=${output} ${texfile}
    whizard-gml "$(basename ${texfile}).mp"
    latex -interaction=nonstopmode -jobname=${output} ${texfile}
    dvips ${output}
}

pushd ${WORK_DIR}
for f in $(find . -name '*.tex'); do
    dir=$(basename $(dirname ${f}))

    make_latex "${dir}-history" ${f}

    rm -f *.aux *.mp *.log *.dvi *.mpx *.ltp *.tmp
done
popd
