#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mincpus=6
#SBATCH --nodes=1
#SBATCH --time=24:00:00
#SBATCH --partition=medium

time $1 $2
