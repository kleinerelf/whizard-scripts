#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mincpus=6
#SBATCH --nodes=1
#SBATCH --time=4-00:00:00
#SBATCH --partition=long

time $1 $2
