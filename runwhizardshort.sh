#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mincpus=6
#SBATCH --nodes=1
#SBATCH --time=2:00:00
#SBATCH -p short

time $1 $2
